<?php
/*///////////////////////////////////////////////////////////////////////
Part of the code from the book 
Building Findable Websites: Web Standards, SEO, and Beyond
by Aarron Walter (aarron@buildingfindablewebsites.com)
http://buildingfindablewebsites.com

Distrbuted under Creative Commons license
http://creativecommons.org/licenses/by-sa/3.0/us/
///////////////////////////////////////////////////////////////////////*/

if(isset($_GET['mainformsubmit'])) {

	// This is validation for non javascript. Add any required variables from your form in here
	if($_GET['email'] == '') {
		$emailError = 'Please enter an email address';
	} else if (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $_GET['email'])) {
		$emailError = 'Please enter a valid email';
	}
	
	if($_GET['fname'] == '') {
			$fnameError = 'Please enter your first name. ';
	}
	
	if($_GET['lname'] == '') {
			$lnameError = 'Please enter your surname.';
	}
	
}
	// Ff there aren't any errors in the form then send a message. Again you need to add any extra feilds to this list
	if(!isset($emailError) && !isset($fnameError) && !isset($lnameError)) {
	
function storeAddress(){
	
	//Use variables to assemble plaintext and html messages for the admin email
	$mailbodyplain = "Hey, you've had an enquiry from the website."."\n";
	$mailbodyplain .= "Name: ".$_GET['fname']." ".$_GET['lname']."\n";
	$mailbodyplain .= "Email: ".$_GET['email']."\n";	
	$mailbodyplain .= "Company: ".$_GET['company']."\n";
	$mailbodyplain .= "Phone: ".$_GET['phone']."\n";
	$mailbodyplain .= "Comment: ".$_GET['comment']."\n";
	if ($_GET['signup'] == "checked") {$mailbodyplain .= "They also signed up to the mailing list."."\n";}	
	
	require_once ($_SERVER['DOCUMENT_ROOT'].'/inc/swift/lib/swift_required.php');
	
	//This is the message sent out by the system to the administrator. It allows you to attach emails and control the comms internally. I am using the Swiftmail library.

	//Create the Transport
	$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com',  465, 'tls')
	 ->setUsername('lolly@sodacreates.co.uk')
	 ->setPassword('kingfisher06')
	  ;
		
	//You could alternatively use a different transport such as Sendmail or Mail:
	
	//Sendmail
	//$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
	
	//Mail
	//$transport = Swift_MailTransport::newInstance();
	
	
	//Create the Mailer using your created Transport
	$mailer = Swift_Mailer::newInstance($transport);
	
	//Create the message
	$message = Swift_Message::newInstance()
	
	
	  //Give the message a subject
	  ->setSubject('Who loves you baby?')
	
	  //Set the From address with an associative array
	  ->setFrom(array('lolly@sodacreates.co.uk' => 'Telly Savalas'))
	
	  //Set the To addresses with an associative array
	  ->setTo(array('hello@sodacreates.co.uk' => 'Soda'))
	
	  //Give it a body
	  ->setBody($mailbodyplain)
	
	  //And optionally an alternative body
	  //->addPart($mailbodyhtml, 'text/html')
	
	  //Optionally add any attachments
	  //->attach(Swift_Attachment::fromPath('my-document.pdf'))
	  ; //This semi colon ends the array above so don't delete it by mistake!
	// Output the message to screen
	//echo $message->toString();
	  
	//Send the message
	$result = $mailer->send($message);
	
	
	if (isset($_GET['signup']) && $_GET['signup'] == "checked") {

	require_once('MCAPI.class.php');
	// grab an API Key from http://admin.mailchimp.com/account/api/
	$api = new MCAPI('cf4e7ddd9dd1c5b3fac3a4a8c7589c24-us1');
	
	// grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
	// Click the "settings" link for the list - the Unique Id is at the bottom of that page. 
	$list_id = "f4b488e2a2";
	
	// Merge variables are the names of all of the fields your mailing list accepts
	// Ex: first name is by default FNAME
	// You can define the names of each merge variable in Lists > click the desired list > list settings > Merge tags for personalization
	// Pass merge values to the API in an array as follows
	$mergeVars = array('FNAME'=>$_GET['fname'],
						'LNAME'=>$_GET['lname'],
						'COMPANY'=>$_GET['company'],
						'PHONE'=>$_GET['phone'],
					);

	if($api->listSubscribe($list_id, $_GET['email'], $mergeVars) === true) {
		// It worked!	
		return 'Please check your email to confirm sign up to our mailing list.';
	}else{
		// An error ocurred, return error message	
		return 'Oops, there was a slight problem with your subscription to our mailing list: ' . $api->errorMessage;
	}
	
}

} // end of validation check


}

// If being called via ajax, autorun the function
if(isset($_GET['ajax'])){ echo storeAddress(); }
?>
