<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Soda is a creative agency. We're in Twickenham. Come and see us.</title>
<meta name="description" content="We work with businesses to help promote their brands, services and products by coming up with great ideas and designs that work."/>
<meta property="og:title" content="Soda is a creative agency!" />
<meta property="og:type" content="company" />
<meta property="og:url" content="http://www.sodacreates.co.uk/" />
<meta property="og:image" content="http://www.sodacreates.co.uk/images/site-graphics/soda-logo.png" />
<meta property="og:site_name" content="Soda is a creative agency in Twickenham" />
<meta property="fb:admins" content="543647624" />
<link rel="stylesheet" href="styles/main.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="styles/colorbox.css" type="text/css" media="screen"/>

<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" href="styles/ie6.css" type="text/css" media="screen"/><![endif]-->
<!-- Begin Typekit -->
	<script type="text/javascript" src="http://use.typekit.com/jgx8zud.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<!-- Begin JavaScript -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script>!window.jQuery && document.write('<script src="/js/jquery-1.4.2.min.js"><\/script>')</script>
		<script type="text/javascript" src="/js/jquery.colorbox.js"></script>	
		<script type="text/javascript" src="/js/jquery.coda-slider-2.0.js"></script>
		<script type="text/javascript" src="/js/jquery.tools.min.js"></script>	
		<script type="text/javascript" src="http://downloads.mailchimp.com/js/jquery.validate.js"></script>
		<script type="text/javascript" src="http://downloads.mailchimp.com/js/jquery.form.js"></script>
		<script type="text/javascript" src="/js/mcform.js"></script>		
		<script type="text/javascript" src="/js/form.js" ></script>
		<script type="text/javascript" src="/js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="/js/jquery.localscroll-1.2.7-min.js"></script>
		<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript">
		 jQuery.noConflict();  
			jQuery().ready(function() {
			
				jQuery('#coda-slider-1').codaSlider();
				jQuery.localScroll({
				duration:1000	
				});
				
				jQuery("img.asterisk[title]").tooltip({
				position: "bottom right", 
				offset: [-60, -135], effect: "slide"
				}); 
				 
				jQuery("#hide").css("display", "none");
				
				jQuery("#banner").css("display", "none");
				
				jQuery("#easteregg-btn").click(function() {
					jQuery("#banner").slideToggle("slow");
				});
				
				jQuery("#revealer").click(function() {
				
				jQuery("#hide").slideToggle("slow");
				jQuery("#mailinglistsignup").slideToggle("slow");							
				
				});
				
				jQuery("#revealer").click(function(){
     			window.location=jQuery(this).find("a").attr("href");
     			return false;
    			});
    			
    			jQuery(".cs_img").click(function(){
     			window.location=jQuery(this).find("a").attr("href");
     			return false;
    			});

				jQuery("a[rel='cs_group']").colorbox();
				
				var url = document.location.href;
				var csnum = document.location.hash;
				if(url.search(/\?portfolio/i) !== -1){
				jQuery("a"+csnum+"[rel='cs_group']").click();
				}
				
				jQuery(document).scrollTop(0);

				
				});
		 </script>
</head>
<body id="index" class="home">
<noscript>
	<div>
        <p>Unfortunately your browser does not have JavaScript capabilities which are required to exploit full functionality of our site. This could be the result of two possible scenarios:</p>
        <ol>
            <li>You are using an old web browser, in which case you should upgrade it to a newer version. We recommend the latest version of <a href="http://www.google.co.uk/chrome">Google Chrome</a>.</li>
            <li>You have disabled JavaScript in you browser, in which case you will have to enable it to properly use our site. <a href="http://www.google.com/support/bin/answer.py?answer=23852">Learn how to enable JavaScript</a>.</li>
        </ol>
    </div>
</noscript>
<!-- facebook like button -->
<div id="fb-soda"><div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#appId=201208693256471&amp;xfbml=1"></script><fb:like href="http://www.facebook.com/sodacreates" send="true" layout="box_count" width="52" show_faces="false" font="lucida grande"></fb:like></div>
<div id="easteregg-btn"></div>
<div id="wrapper">
<header id="banner" class="body">
<img src="images/site-graphics/lolly-top.png" alt="Top of a lolly drawn by Aled Lewis" title="This is a lolly as drawn by Aled Lewis Esquire" width="836" height="180" />
</header><!-- /#banner -->
  <!--[if lte IE 6]><section id="ie6" class="body"> 
   <div id="ie6-warning" class="body">Hello. It seems you are using the Internet Explorer 6 browser. We're sorry to see that you are having to still use it as there are so many cool things we can do in newer browsers like <a href="http://www.google.com/chrome" title="Chrome browser">Chrome</a> or <a href="http://www.mozilla-europe.org/en/firefox/" title="Firefox browser">Firefox</a>. Unfortunately your browser won't play nicely with some of the nifty bits on our website. Happily, you can access our contact information here until that frabjous day when you or, most probably, your IT department upgrade your browser. In the meantime <a href="http://www.flickr.com/photos/sodacreates/" title="Our work">here are samples of our work</a> and you can <a href="http://blog.sodacreates.co.uk" title="Our thoughts">read our blog here.</a></div>
   <div id="ie6-intro">
						<h1 class="tk-ff-tisa-web-pro">Soda is a creative agency.</h1>
						<p class="tk-deva-ideal">We work with businesses to help promote their brands, services and products.</p>
						<p>We believe that strong ideas and great designs are vital components of an effective marketing strategy.</p>
						<p>We also believe that continual measurement of your marketing strategy’s performance is essential to building a successful business.</p>
					</div>
					</section><![endif]--><!-- /#ie6-intro -->

   
<section id="content" class="body">
	<div class="coda-slider preload" id="coda-slider-1">
		<ol id="posts-list" class="hfeed coda-slider-wrapper">
			<li>
				<article class="hentry panel">
					<div class="entry-header"><img src="images/site-graphics/header-we.png" class="asterisk" alt="Header graphic saying we" title="*Sorry, we love doing what we do so much that we sometimes get a bit carried away." width="660" height="180" /></div>
					<div class="entry-content">
						<h1 class="tk-ff-tisa-web-pro">Soda is a creative agency.</h1>
						<p>We are a close-knit team of creative people<br />who are passionate about what we do.</p>
						<p>We are friendly, easy to work with and full of ideas that will help bring success to your business.</p>
					</div><!-- /.entry-content -->
				</article>
			</li>	
			<li>
				<article class="hentry panel">
					<div class="entry-header"><img src="images/site-graphics/header-create.png" alt="Header graphic saying create" width="660" height="180" /></div>
					<div class="entry-content">
						<h1 class="tk-ff-tisa-web-pro">Soda has great ideas.</h1>
						<p>We create effective marketing strategies and design for print, online, branding, advertising and events.</p> 
						<p>We’ll keep you up-to-date with all the latest marketing trends  and beautiful graphic design.</p>
					</div><!-- /.entry-content -->
				</article>
			</li>
			<li>
				<article class="hentry panel">
					<div class="entry-header"><img src="images/site-graphics/header-and.png" class="asterisk" alt="Header graphic saying and" title="*Did you know it was Tiro, Cicero's secretary, who invented the ampersand, that little symbol we still use today." width="660" height="180" /></div>
					<div class="entry-content">
						<h1 class="tk-ff-tisa-web-pro">Soda works with you.</h1>
						<p>We know the value of developing strong working relationships with our clients.</p>
						<p>We work with marketing teams, or as your own dedicated design and marketing department.</p>
					</div><!-- /.entry-content -->
				</article>
			</li>
			<li>
				<article class="hentry panel">
					<div class="entry-header"><img src="images/site-graphics/header-build.png" alt="Head graphic saying build" width="660" height="180" /></div>
					<div class="entry-content">
						<h1 class="tk-ff-tisa-web-pro">Soda builds brands.</h1>
						<p>We believe in building: Building relationships,<br />building knowledge and building successful brands.</p>  
						<p>Whether you need a one-off project, or a long-term marketing strategy, we can build it together.</p>
					</div><!-- /.entry-content -->
				</article>
			</li>
			<li>
				<article class="hentry panel">
					<div class="entry-header"><img src="images/site-graphics/header-success.png" alt="Head graphic saying success" width="660" height="180" /></div>
					<div class="entry-content">
						<h1 class="tk-ff-tisa-web-pro">Soda loves great design.</h1>
						<p>We know that when you mix great ideas together with great design you get results.
						<p>Good design costs the same as bad design. And we know which one you’d prefer.</p>
					</div><!-- /.entry-content -->
				</article>
			</li>
		</ol><!-- /#posts-list -->
	</div><!-- /#coda-slider -->
		<div id="casestudies">
			<h1>Our latest case studies</h1>
			<ul class="cs_gallery">
				<li class="cs_link"><a href="casestudies/commercial-property-marketing.html" rel="cs_group" id="cs_5" title="Windows and brochure design by Soda for St James"><img src="/images/casestudies/cs_st_james_tmb.png" alt="Property development window and brochure design by Soda" title="Property development window and brochure design by Soda" width="134" height="99" /></a>
				<h2>Brochure &amp; window displays for St James</h2>
				</li>
				<li class="cs_link" id="end"><a href="casestudies/site-branding.html" rel="cs_group" id="cs_6" title="Octink's website content is managed by Soda"><img src="images/casestudies/cs_trinity_village_tmb.png" alt="Soda designs branding and brochures for Ward Homes" title="Soda designed the branding for Trinity Village" width="134" height="99" /></a>
				<h2>Branding for developer Ward Homes</h2>
				</li>
				<li class="cs_link"><a href="casestudies/email-marketing.html" rel="cs_group" id="cs_7" title="Soda design and manage HTML email marketing campaigns for Octink"><img src="images/casestudies/cs_octink_email_campaign_tmb.png" alt="Soda used MailChimp to power an email campaign for Octink" title="Soda devised an html email campaign for Octink" width="134" height="99" /></a>
				<h2>Email marketing campaign for Octink</h2>
				</li>
				<li class="cs_link"><a href="casestudies/illustration-and-design.html" rel="cs_group" id="cs_8" title="Archers marketing material featuring illustrations by Aled Lewis"><img src="images/casestudies/cs_archers_tmb.png" alt="by Soda" title="Archers credentials document" width="134" height="99" /></a>
				<h2>Website &amp; brochure for local signmaker</h2>
				</li>
			</ul>
			<ul class="cs_gallery">
				<li class="cs_link"><a href="casestudies/property-marketing.html" rel="cs_group" id="cs_1" title="Soda designed branding for Waterside Park, a Barratt development"><img src="images/site-graphics/cs_wp_tmb.jpg" alt="Waterside Park logo designed by Soda" title="Soda designed branding for Waterside Park, a Barratt development" width="134" height="99" /></a>
				<h2>Branding for property development by Barratt</h2>
				</li>
				<li class="cs_link"><a href="casestudies/local-business-websites.html" rel="cs_group" id="cs_2" title="Vintage Simplicity website screenshot"><img src="images/site-graphics/cs_vs_tmb.jpg" alt="Local business website by Soda" title="Vintage Simplicity website screenshot" width="134" height="99" /></a>
				<h2>Website for local start-up vintage business</h2>
				</li>
				<li class="cs_link"><a href="casestudies/poster-design.html" rel="cs_group" id="cs_3" title="Poster design for iQ by Soda"><img src="images/site-graphics/cs_iq_tmb.jpg" alt="Poster design by Soda" title="Poster design for iQ Accommodation by Soda" width="134" height="99" /></a>
				<h2>Poster campaign for iQ Student Accommodation </h2>
				</li>
				<li class="cs_link" id="end"><a href="casestudies/content-management.html" rel="cs_group" id="cs_4" title="Octink website content is managed by Soda"><img src="images/site-graphics/cs_oct_tmb.jpg" alt="Octink website content is managed by Soda" title="Octink website content is managed by Soda" width="134" height="99" /></a>
				<h2>Content strategy &amp; site maintenance for Octink</h2>
				</li>
			</ul>
		</div><!-- /casestudies -->
		<div id="branding">
			<img src="images/site-graphics/soda-logo.png" alt="Soda is a creative agency. This is our logo!" title="Soda is a creative agency. This is our logo!" width="74" height="74" class="sodalogo" />
			<img src="images/site-graphics/create-and-build-success.png" width="380" height="35" class="strapline" alt="Create and build success strapline graphic" title="We create and build success"/>
		</div><!-- /#branding -->		
</section><!-- /#content -->
	<div id="contactbtn">
		<div id="revealer"><a href="#mailinglist" title="This button reveals more information about Soda.">More info</a></div>
	</div><!-- /#contactbtn -->
	<div id="hide">
<section id="contact" class="body">
<div class="info">
<h2>Get in touch.</h2>
<p>Help yourself to <a href="/images/assets/Soda_A4briefnewbus_pages_dev.doc" title="Soda brief sheet" onClick="_gaq.push(['_trackEvent', 'DOC', 'Download', 'Soda new business brief']);" >our (very) brief sheet</a>. It's an easy way to prepare if you'd like to come and see us for a free consultation.</p>
<p>You can find out more about Soda in <a href="http://issuu.com/JonathanElliman/docs/a_little_brochure_by_soda" title="A lovely little brochure by Soda" onClick="_gaq.push(['_trackEvent', 'View', 'Issuu', 'Soda brochure 2011']);" >our lovely little brochure</a>. If you want to see more samples of our work, just fill out the form below and we'll send you a lovely printed brochure.</p>
<p>You can also <a href="http://flickr.com/photos/sodacreates/" title="Our work">look on Soda's Flickr photostream</a> for more examples of our work.</p>
</div>
<form id="getintouch" name="getintouch" class="getintouch" autocomplete="on" enctype="multipart/form-data" method="get" action="">
				<div id="response">
					<?php require_once('inc/mc/process.php');
					if(isset($_GET['mainformsubmit']) && function_exists('storeAddress')){
					echo '<div id="success"><h1>Thanks!</h1><p>Your email was sent successfully.</p></div>'.'<p>'. storeAddress() . '</p>';
					 } ?>
				</div>
	<div id="formholder">
	<div id="toprow">
	<fieldset id="person">
		<label for="fname"><span>First name *</span><input type="text" id="fname" name="fname" value="" placeholder="Enter your first name" tabindex="1" /><?php if(isset($fnameError)) echo '<span class="error">'.$fnameError.'</span>'; ?></label>
		<label for="lname"><span>Last name *</span><input type="text" id="lname" name="lname" value="" placeholder="Enter your last name" tabindex="2" /><?php if(isset($lnameError)) echo '<span class="error">'.$lnameError.'</span>'; ?></label>
		<label for="email"><span>Email address *</span><input type="text" id="email" name="email" value="" placeholder="Enter your email address" tabindex="3" /><?php if(isset($emailError)) echo '<span class="error">'.$emailError.'</span>'; ?></label>
	</fieldset>
	<fieldset id="business">
		<label for="company"><span>Company name</span><input type="text" id="company" name="company" value="" placeholder="Enter your company name" tabindex="4" /></label>
		<label for="phone"><span>Phone number</span><input type="text" id="phone" name="phone" value="" placeholder="Enter your phone number" tabindex="5" /></label>
	</fieldset>
	</div>
	<div id="bottomrow">
	<fieldset id="comments">
		<label for="comment"><span>Comments</span><textarea id="comment" name="comment" placeholder="Leave a comment here" tabindex="6"></textarea></label>
	</fieldset>
	<fieldset id="signup-container">
		<label for="signup"><span>I'd like to sign up to a regular email newsletter brimming with creative ideas.</span><input type="checkbox" name="signup" id="signup" value="checked" tabindex="7"/></label>
	</fieldset>
	<fieldset class="button">
		<input type="submit" id="mainformsubmit" name="mainformsubmit" value="Send message!" tabindex="7"/>
	</fieldset>
	</div>
	</div><!-- /formholder -->
</form>
	</section>
</div><!-- / #hide -->
<section id="contentinfo" class="body">
	<div id="mailinglistsignup">
	<div id="calltoaction">
		<p>Sign up to our mailing list for a regular newsletter brimming with creative ideas and advice.</p>
		<p>We won't share this data with anybody else. Not even our mums.</p>
	</div>
<!-- Sign up form -->
		<div id="mailinglist">
<form id="newsletter" name="newsletter" class="validate" autocomplete="on" enctype="multipart/form-data" method="post" action="http://soda-art.us1.list-manage.com/subscribe/post?u=524fe1124cd76d6c1be1a6715&amp;id=f4b488e2a2" target="_blank">
	<fieldset>
		<label for="mce-EMAIL"><span>Email address *</span><input type="text" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Enter your email address" tabindex="8"></label>
		<div id="mce-responses">
			<div class="response" id="mce-error-response" style="display:none"></div>
			<div class="response" id="mce-success-response" style="display:none"></div>
		</div>
	</fieldset>
	<fieldset>
		<input type="submit" id="submit" name="submit" value="Sign me up!" tabindex="9" class="btn noborder" />
	</fieldset>
</form>	
		</div>
	</div><!-- /#mailinglistsignup -->
</section><!-- /#contentinfo -->
<section id="address" class="body">
		<div id="info" class="vcard">
			<div class="adr">
			<span class="org">Soda</span>, <span class="street-address">80 Crown Road</span>, <span class="locality">St Margarets</span>, <span class="region">Middlesex</span> <span class="postal-code">TW1 3ER</span><span class="map"><a href="http://g.co/maps/bm2e7" title="Get directions to Soda in Twickenham">[MAP]</a></span></div>
  <div>Telephone: <span class="tel"><span class="type">Work</span> <span class="value">020 8891 4278</span></span> Facsimile: <span class="tel"><span class="type">Fax</span> <span class="value">020 8711 3871</span></span>Email: <a href="mailto:hello@sodacreates.co.uk" class="email" title="We love emails so send us one">hello@sodacreates.co.uk</a></div>
 </div>
</section>
<section id="featured" class="body">
	 <ul>
		<li id="flickr-box">
			<article>
				<h2>More work</h2>
					<div class="smlink">
						<a href="http://www.flickr.com/photos/sodacreates/" title="Our work">Flickr</a>
					</div>
			</article>
		</li>
		<li id="wp-box">
			<article>
				<h2>More news</h2>
					<div class="smlink">
						<a href="http://blog.sodacreates.co.uk" title="Our thoughts">Wordpress</a>
					</div>
			</article>
		</li>
		<li id="linkedin-box">
			<article>
				<h2>More us</h2>
					<div class="smlink">
						<a href="http://www.linkedin.com/companies/soda-art-ltd?trk=fc_badge" title="Our LinkedIn profiles">LinkedIn</a>
					</div>
			</article>
		</li>
		<li id="facebook-box">
			<article>
				<h2>Even more us</h2>
					<div class="smlink">
						<a href="http://facebook.com/sodacreates" title="Our Facebook profile">Facebook</a>
					</div>
			</article>
		</li>
		<li id="twitter-box">
			<article>
				<h2>Moronic banter</h2>
					<div class="smlink">
						<a href="http://twitter.com/sodasays" title="Our tweets">Twitter</a>
					</div>
			</article>
		</li>
	</ul>
	</section><!-- /#featured -->
	<div id="lolly-bottom"><img src="images/site-graphics/highlight-bottom.png"  width="15" height="10" alt="Bottom of the lolly"/></div>
</div><!-- /#wrapper -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19436689-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>