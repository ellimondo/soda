//Jquery to validate and process a form
jQuery.noConflict();
jQuery(document).ready(function(){

	//This listens for the form button and submits on a click
	jQuery("#mainformsubmit").click(function(){					   				   
		jQuery(".error").hide();
		//This validates the email address
		var hasError = false;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var emailVal = jQuery("#email").val();
		if(emailVal == '') {
			jQuery("#email").css('border','2px solid #e41d8c');
			jQuery("#email").after('<span class="error">Please enter an email address.</span>');
			hasError = true;
		} else if(!emailReg.test(emailVal)) {	
			jQuery("#email").css('border','2px solid #e41d8c');
			jQuery("#email").after('<span class="error">Please enter a valid email address.</span>');
			hasError = true;
		} else {	
			jQuery("#email").css('border','2px solid #a81567');			
		}
		//These sections of code check if fields are empty. Copy one and use the id of the input field for extra required fields
		var nameVal = jQuery("#name").val();
		if(nameVal == '') {
			jQuery("#name").css('border','2px solid #e41d8c');
			jQuery("#name").after('<span class="error">Please enter your first name.</span>');
			hasError = true;
		} else {	
			jQuery("#name").css('border','2px solid #a81567');	
		}
		
		var lnameVal = jQuery("#lname").val();
		if(lnameVal == '') {
			jQuery("#lname").css('border','2px solid #e41d8c');
			jQuery("#lname").after('<span class="error">Please enter your last name.</span>');
			hasError = true;
		} else {	
			jQuery("#lname").css('border','2px solid #a81567');
		}
		
		//This bit assembles and passes the field contents into the sendEmail.php file
		if(hasError == false) {
			jQuery(this).hide();
			jQuery("#formholder div.buttons").append('<img src="./assets/loading.gif" alt="Loading" id="loading" />');
			//You'll need to enter any new fields into this list using field ids
			var phoneVal = jQuery("#phone").val();
			var companyVal = jQuery("#company").val();	
			var commentVal = jQuery("#comment").val();			
			if(jQuery('#gitnews').attr('checked') == true) {
			var newsletterVal = "checked"; 
			}else {
			var newsletterVal = "unchecked";
			}	
			
			//And also enter any new fields into this list too or they won't make it to the email
			jQuery.post("sendEmail.php",
   				{ email: emailVal, name: nameVal, lname: lnameVal, phone: phoneVal, company: companyVal, newsletter: newsletterVal, comment: commentVal},
   					function(data){
   						//This hides the form once submitted
						jQuery("#formholder").slideUp("slow", function() {	
							if(jQuery('#gitnews').attr('checked') == true) {
									jQuery("#formholder").before('<div id="success"><h1>Thanks!</h1><p>Your email was sent.</p><p>To complete the subscription process, please click the link in the email we just sent you as we need to confirm your email address.</p></div>'); 
			}else {
									jQuery("#formholder").before('<div id="success"><h1>Thanks!</h1><p>Your email was sent.</p></div>');
			}			   
																
						});						
   					}
				 );
		}
		
		return false;
	});						   
});