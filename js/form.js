/*///////////////////////////////////////////////////////////////////////
Ported to jquery from prototype by Joel Lisenby (joel.lisenby@gmail.com)
http://joellisenby.com

original prototype code by Aarron Walter (aarron@buildingfindablewebsites.com)
http://buildingfindablewebsites.com

Distrbuted under Creative Commons license
http://creativecommons.org/licenses/by-sa/3.0/us/
///////////////////////////////////////////////////////////////////////*/
  
jQuery(document).ready(function() {
jQuery.noConflict();
	jQuery('#getintouch').submit(function() {
	
	//These sections of code check if fields are empty. Copy one and use the id of the input field for extra required fields
		jQuery(".error").hide();
		
		var hasError = false;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var emailVal = jQuery("#email").val();
		if(emailVal == '') {
			jQuery("#email").after('<span class="error">Please enter an email address.</span>');
			hasError = true;
		} else if(!emailReg.test(emailVal)) {	
			jQuery("#email").after('<span class="error">Please enter a valid email address.</span>');
			hasError = true;
		}
		//These sections of code check if fields are empty. Copy one and use the id of the input field for extra required fields
		var fnameVal = jQuery("#fname").val();
		if(fnameVal == '') {
			jQuery("#fname").after('<span class="error">Please enter your first name.</span>');
			hasError = true;
		}
		
		var lnameVal = jQuery("#lname").val();
		if(lnameVal == '') {
			jQuery("#lname").after('<span class="error">Please enter your surname.</span>');
			hasError = true;
		}		
	
		// update user interface
	
				if(jQuery('#signup').attr('checked') == true) {
			var signupVal = "checked"; 
			}else {
			var signupVal = "unchecked";
			}

		// Prepare query string and send AJAX request
		
		if(hasError == false) {
		jQuery("fieldset.buttons").append('<img src="/images/site-graphics/loading.gif" alt="Loading" id="loading" />');
		jQuery.ajax({
			url: '/inc/mc/process.php',
			data: 'ajax=true&email=' + escape(jQuery('#email').val()) + '&fname=' + escape(jQuery('#fname').val()) + '&lname=' + escape(jQuery('#lname').val()) + '&company=' + escape(jQuery('#company').val()) + '&phone=' + escape(jQuery('#phone').val()) + '&comment=' + escape(jQuery('#comment').val()) + '&signup=' + signupVal,
			success: function(msg) {
				
				jQuery("#contactus").slideUp("slow", function() {	
							if(jQuery('#signup').attr('checked') == true) {
								jQuery('#response').html(msg);
									
									jQuery("#response").before('<div id="success"><h1>Thanks!</h1><p>Your message was sent successfully. Thanks for popping by!</p></div>'); 
									jQuery('#response').addClass('mccallback');
			}else {
									jQuery("#response").before('<div id="success"><h1>Thanks!</h1><p>Your message was sent successfully. Thanks for popping by!</p></div>');
			}			   
																
						});			
				
			}
		});
	}
		return false;
	});
	
});